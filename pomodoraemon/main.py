"""
The command line script.
"""

import logging
import argparse

logger = logging.getLogger(__name__)

from asciimatics.effects import Cycle, Stars, Sprite
from asciimatics.renderers import FigletText
from asciimatics.scene import Scene
from asciimatics.screen import Screen
from asciimatics.paths import Path
from asciimatics.renderers import StaticRenderer
import math
import asciimatics

def demo(screen):
    effects = [
        Cycle(
            screen,
            FigletText("POMODORAEMON", font='big'),
            int(screen.height / 2 - 8)),
        Stars(screen, 200)
    ]
    screen.play([Scene(effects, 500)])

def main():
    """
    Run the CLI.
    """

    # logger.basicConfig(level=logging.INFO)
    Screen.wrapper(demo)
    #parser = argparse.ArgumentParser(description="Run a pomodoro timer.")

if __name__ == "__main__":
    main()
